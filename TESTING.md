# Testing Specifications

## Boundary Value Testing
* We performed Boundary Value Testing following the robust technique and primarily focusing on the RunSimulation() function. This was a way to test any defects in relation with boundary related numbers. 

## Equivalence Class Testing
* We perfomed Equivalence Class Testing as a method where inputs are grouped into classes that are expected to behave the same way. Testing just one input from each class helps ensure comprehensive coverage and efficiency in identifying potential issues. This technique is really helpful as it has a way of efficiently testing and reducing the number of test cases needed to cover a wide range of scenarios, ultimately leading to more robust and reliable program with the Howitzer simulator. 

## Decision Table Based Testing
* We perfomed Desicion Table-Based testing to make sure that we had used a testing system that utilised a way to check for various different input combinations. An example for this could be that all the variables inputted are true, that would mean that the expected answer is also true. However, if there is a negative angle, that would mean that the exected answer is now an error. In saying this, the testing again was done primarily on the RunSimlation() function. 

## Path Testing
* We performed Path Testing as a way to focus on systematically testing different paths or sequences of code execution within a program. Our aim with this testig method was to ensure that all possible routes through a the Simulator class were exercised during testing. By testing various paths, we are able to uncover potential logic errors, bugs, and unexpected interactions between different parts of the code.

## Dataflow Testing
* We performed Dataflow Testing as a way to identify how data is processed, modified, and transferred through different parts of our code. By examining this data movement, we can easily can uncover potential issues like uninitialized variables, incorrect assignments, or data dependencies that could lead to errors or unexpected behavior.

## Slice Testing
* We performed Slice Testing so we could isolate and test specific portions of code that are relevant to a particular functionality or feature. This would involve creating code "slices" by extracting the relevant parts of the Simulator class, because of this we can thoroughly test the behavior of individual features without the complexity of the entire program. This approach helps identify bugs and issues related to specific functionalities, making troubleshooting and debugging more targeted and efficient.

## Integration Testing
* We performed Integration Testing following the Top Down Approach, we performed it on the Point class which utilizes the Velocity and Acceleration classes in one of its functions (the function is the main purpose of the class) and so we created test stubs to test the call of the function using incomplete Velocity and Acceleration classes, almost like were dummy classes or as we more commonly know them: test stubs.

## System Testing
* We performed System Testing (in TestSimulator.java) in order to test the complete functionality and integration of the system, ensuring the outputs are correct. We implemented this as a Finite State Machine where each node (function) was covered by running the system, **utilizing all of the units** in the simulator system to get our results.
