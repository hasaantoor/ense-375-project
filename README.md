# ENSE 375 Project

## Team Members
* Hasaan Toor
* Aryan Chandra
* Meklit Alemu

## Problem Specification
##### A Howitzer company is asking for a simulator to teach the soldiers about kinematics and dynamics. They would like to simulate the Howitzer shooting a projectile across an empty field.

<details><summary>Additional Background Information</summary>

Some possible variables to the simulator are the barrel pose (position and orientation), radius and mass of the projectile, drag coefficient, initial speed, force applied to the projectile, and gravity. 

The rigid body kinematics and dynamics of any object expressed in an inertial frame are:

> p(t) = v(t)

> mv(t) = f(t) + f<sub>d</sub>(t) + mg

Where, p ∈ ℝ<sup>3</sup> is the position of the projectile, v ∈ ℝ<sup>3</sup> is the linear velocity of the projectile, m ∈ ℝ is the mass of the projectile, g ∈ ℝ<sup>3</sup> is the acceleration due to gravity, f ∈ ℝ<sup>3</sup> is the external force acting on the object, and f<sub>d</sub> ∈ ℝ<sup>3</sup> is the drag force acting on the object.

In fluid dynamics, the drag force is:

> f<sub>d</sub>(t) = - 1/2 CpAu(t)<sup>2</sup>

Where, C ∈ ℝ<sup>+</sup> is the drag coefficient, p ∈ ℝ<sup>+</sup> is the density of the fluid, A ∈ ℝ<sup>+</sup> is the area of the
projectile facing the fluid, and u ∈ ℝ<sup>3</sup> is the flow velocity relative to the projectile. 

</details>

## Design Requirements
### Objectives
* Implement prototype simulator of a Howitzer artillery piece.
* Design test cases for the simulator using:
> * Boundary Value Testing
> * Equivalence Class Testing
> * Decision Table-Based Testing
> * Path Testing
> * Dataflow
> * Slice Testing
* Perform Integration Testing.
* Perform System Testing.
### Constraints
* <b>Reliability</b>
> * <b>Accuracy</b>: The simlulator should be able to make accurate and reliable simulations of the Howitzer firing, and taking into account the laws of physics and the variables mentioned. 
> * <b>Statbility</b>: The simulator should be stable and perform consistently without crashed or unexpected behaviour during training sessions or tests. 
> * <b>Error Handling</b>: The simulator should have excellent error handling mechanisms to handle exceptions, input validation, and unexpected scenarios fluently. 

* <b>Sustainability</b>
> * <b>Resource Utilisation</b>: The simulator should be designed to efficiently utilise system resources such as CPU, memory, and storage to minimise energy consumption and maximise performance. 
> * <b>Scalability</b>: The simulator should be designed to accommodate future enhancements and scalability requirements without requiring significant changes to the underlying system build. 
> * <b>Compatibility</b>: The simluator should be compatible with a wide range of hardware configurations and operating systems to minimise the need for frequent upgrades or replacements.  

* <b>Ethics</b>
> * <b>Ethical Use</b>: The simulator should be used for lawful and ethical purposes, abiding to legal regulations and making sure that it is not used to promose violence, harm, or anything along illegal activities. 
> * <b>Privary and Data Protection</b>: The simulator should handle any personal or sensitive data with proper care, making sure proper encryption and compliance with relevant privary regulations. 
> * <b>Fairness and Bias</b>: The simulator should be designed and trained to be fair and unbiased, avoiding any discriminatory or prejudice behaviour.

* <b>Regulatory Compliance (Security and Access)</b>
> * <b>Laws and regulations</b>: The simulator should abide by the bodies of laws and standards that govern the engineering and software development industry as well as the consequences for not following them. 
> * <b>Contracts</b>: The simulator and developers have an obligation of upholding the terms of their agreements. The system should comply with the qualification standards outlined in the agreement.
> * <b>Security and access</b>: The simulator should balance security and efficiency to prevent unforeseen vulnerabilities and yield good performance. It should integrate layered regular updates, testing, secure configuration, and code review to provide security over the data, framework, and network communications. Access to the software system should be limited to authorized personnel.

* <b>Economic Factors</b>
> * <b>Project Budget</b>: The simulator should have a budget that is adequate and appropriately allocated to ensure high product quality and performance.
> * <b>Resource Limitations</b>: The simulator should be produced within the established boundaries and availability of materials, time, people, manpower, and capital resources. 
> * <b>Cost-Benefit Analysis</b>: The simulator should be designed after systematic evaluation of the benefits and costs of the design. It should consider the the level of competition between consultants, contractors and other suppliers.

* <b>Societal Impacts</b>
> * <b>Adequate Research</b>: The simulator should be designed after sufficient research to understand the function, status, social value of the product for the consumer and community. The research should investigate the Howitzer company, outline impacts of producing the product, delivering the product to the consumer, and the use of the product on society.
> * <b>Social Considerations</b>: The simulator should incorporate various techniques to create a fail-safe design that mitigates losses due to user or system error. It should limit use of bad judgments, inadequate assumptions, and poor design principles that leads to future devastative effects.
> * <b>Risk Analysis</b>: The simulator should be designed after rigorous anysis of the design's impact on the target market, public facilities, military usage, stakeholders, public perception, and organizational conduct.

## Installation and Use Instructions
<details><summary>Detailed Installation</summary>

Majority of the simulation tests are completed as JUnit test. Therefore, it's best to use a integrated development environment (IDE) that supports JUnit test. We used Eclipse IDE for Java Developers - 2023-03.

Download here: https://www.eclipse.org/downloads/

Install / clone repository as you would normally install on your IDE.

GitLab: - How to Connect Eclipse To GitLab: https://www.youtube.com/watch?v=WaABPJCYytA

</details>
<details><summary>Use Instructions</summary>

The Project Layout

> src : Where the project simulation files are found    

> testSimulator: Where the test JUnit files are found

To run JUnit tests:

> right-click, run-as, JUnit

> set up configuration for JUnit

You'll see pass/fail for JUnit test cases. You'll also see projectile trajectory x, y, z axis output in console.

</details>