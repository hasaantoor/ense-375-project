package Simulator;

public class Point {
	double time;	
	Position position;
	Velocity velocity;

	public Point(double initialZ, double initialV) {
		this.time = 0;
		this.position = new Position(initialZ);
		this.velocity = new Velocity(initialV);
	};
	
	public Point GetValues(double time, Velocity v0, double initialZ, double[] acceleration) {
		this.time = time;
		this.position.finalX = position.CalculatePositionX(time, v0.vx, acceleration[0]);
		this.position.finalY = position.CalculatePositionY(time, v0.vy, acceleration[1]);		
		this.position.finalZ = position.CalculatePositionZ(time, v0.vz, acceleration[2]);		

		this.velocity.vx = velocity.CalculateVelocity(time, v0.vx, acceleration[0]);		
		this.velocity.vy = velocity.CalculateVelocity(time, v0.vy, acceleration[1]);		
		this.velocity.vz = velocity.CalculateVelocity(time, v0.vz, acceleration[2]);
		return this;
	};
	
}
