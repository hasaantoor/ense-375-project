package Simulator;

public class Simulator {
	public static final double	GRAVITY_ACCELERATION = 9.81;
	Point projectilePoint; 
    Velocity v0;
    
	public Simulator (double a, double b, double c) {
		boolean validAngles = checkValidAngles(a, b, c);
		if(validAngles) {
			this.projectilePoint = new Point(0, 0);
		    this.v0 = new Velocity(0);
		}
		else {
			throw new IllegalArgumentException("Angles are not valid.");
		}
	};
	
	private boolean checkValidAngles(double a, double b, double c) {
        if (a >= 0 && b >= 0 && c >= 0) {
			a = Math.toRadians(a);
			b = Math.toRadians(b);
			c = Math.toRadians(c);
			int checkAngles = (int) Math.round(Math.pow(Math.cos(a), 2) + Math.pow(Math.cos(b), 2) + Math.pow(Math.cos(b), 2));
			return (checkAngles == 1);
			}
			else return false;
    };
	
	public double CalculateTotalTime(double initialPositionZ, double[] acceleration) {
	    double a = 0.5 * acceleration[2], b =  v0.vy, c = initialPositionZ;
	    double root1, root2 = 0;

	    double determinant = (b * b) - (4 * a * c);

	    if (determinant > 0) {
	      root1 = (-b + Math.sqrt(determinant)) / (2 * a);
	      root2 = (-b - Math.sqrt(determinant)) / (2 * a);
	    }
	    
	    else if (determinant == 0) {
	      root1 = root2 = -b / (2 * a);
	    }
	    else {
	      root1 = 0;
	    }
		
		return (root1 > 0 ? root1 : root2);
	};
	
	public double[] CalculateDrag (double dragCoefficent, double fluidDensity, double projectileArea, double flowVelocity, double ballMass)
	{	

		double dragX = -0.5 * dragCoefficent * fluidDensity * projectileArea * Math.pow(v0.vx, 2);
		double dragY = -0.5 * dragCoefficent * fluidDensity * projectileArea * Math.pow(v0.vy, 2);
		double dragZ = -0.5 * dragCoefficent * fluidDensity * projectileArea * Math.pow(v0.vz, 2);
        
        double[] force = new double[3];
        force[0] = dragX;
        force[1] = dragY;
        force[2] = (-ballMass * Simulator.GRAVITY_ACCELERATION) + dragZ;
        	
        double[] acceleration = new double[3];
        acceleration[0] = force[0]/ballMass;
        acceleration[1] = force[1]/ballMass;
        acceleration[2] = force[2]/ballMass;
        
		return acceleration;
	};
	
	public void RunSimulation(double a, double b, double c, double height, double initialVelocity, double dragCoefficent, double fluidDensity, double ballDiameter, double ballMass) 
	{
		boolean validAngles = checkValidAngles(a, b, c);
		if (validAngles && height >= 0 && initialVelocity >= 0 && dragCoefficent >= 0 && fluidDensity >= 0 && ballDiameter >= 0 && ballMass >= 0) {
			v0.initial = initialVelocity;
			v0.vx = v0.CalculateVelocityComponenet(initialVelocity, a);
			v0.vy = v0.CalculateVelocityComponenet(initialVelocity, b);
			v0.vz = v0.CalculateVelocityComponenet(initialVelocity, c);
			double projectileArea = 0.25 * Math.PI * Math.pow(ballDiameter, 2);
			double[] acceleration = CalculateDrag(dragCoefficent, fluidDensity, projectileArea, initialVelocity, ballMass);
			ShowResults(height, acceleration);
		} else {
			throw new IllegalArgumentException("Invalid input parameters.");
		}
	};
	
	public void ShowResults(double initialHeight, double[] acceleration) {
        System.out.println("Drag and g");
        System.out.printf("%-20s %-30s %-30s %-30s%n", "Time", "Distance-X", "Distance-Y", "Height-Z");
        double getTotalProjectileTime = CalculateTotalTime(initialHeight, acceleration);

		for(double time = 0; time <= getTotalProjectileTime; time += 0.5) {
			projectilePoint.GetValues(time, v0, initialHeight, acceleration);	        
			System.out.printf("%-20s %-30s %-30s %-30s%n", time, projectilePoint.position.finalX, projectilePoint.position.finalY, projectilePoint.position.finalZ);
		}
	}
	
}
