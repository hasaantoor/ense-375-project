package Simulator;

public class Velocity {
	double initial;
	double vx;
	double vy;
	double vz;	
	
	public Velocity (double initial) {
		this.initial = initial;
	};
	
	public double CalculateVelocityComponenet (double velocity, double angle) {
  
        double radians = Math.toRadians(angle);
		return velocity * Math.cos(radians);
	};
	
	public double CalculateVelocity (double time, double initialVelocity, double acceleration) {
		  
		return initialVelocity + (acceleration * time);
	};
}
