package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BoundaryValueTesting 
{
	private Simulator simulator;

    @BeforeEach
    public void setUp() {
        simulator = new Simulator(70.52, 48.19, 48.2);
    }

    @Test
    public void testValidAnglesBoundary() {
        // Test with valid angles and all other inputs within valid ranges
        assertDoesNotThrow(() -> simulator.RunSimulation(70.52, 48.19, 48.2, 100.0, 30.0, 0.47, 1.225, 0.18, 17.6));

        // Test with invalid negative angles
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(-70.52, 48.19, 48.2, 100.0, 30.0, 0.47, 1.225, 0.18, 17.6));
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(70.52, -48.19, 48.2, 100.0, 30.0, 0.47, 1.225, 0.18, 17.6));
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(70.52, 48.19, -48.2, 100.0, 30.0, 0.47, 1.225, 0.18, 17.6));
    }

    @Test
    public void testHeightBoundary() {
        // Test with valid height
        assertDoesNotThrow(() -> simulator.RunSimulation(70.52, 48.19, 48.2, 0.0, 50.0, 0.2, 1.2, 0.2, 0.05));
        assertDoesNotThrow(() -> simulator.RunSimulation(70.52, 48.19, 48.2, 200.0, 30.0, 0.47, 1.225, 0.18, 17.6));

        // Test with invalid negative height
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(70.52, 48.19, 48.2, -0.1, 50.0, 0.2, 1.2, 0.2, 0.05));
    }
}
