package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SliceTest 
{
    private Simulator simulator;

    /*
     *Create a new Simulator instance before each test
     */
    @BeforeEach
    public void setUp() {

        simulator = new Simulator(70.52, 48.19, 48.2);
    }
    /*
     * Test with valid angels
     */
    @Test
    public void testValidAngles() 
    {
        assertDoesNotThrow(() -> {
            simulator.RunSimulation(70.52, 48.19, 48.2, 10, 13, 0.25, 1.225, 0.075, 17.6);
        });
    }

    /*
     * test with invalid angles
     */
    @Test
    public void testInvalidAngles() 
    {
        assertThrows(IllegalArgumentException.class, () -> {
            simulator.RunSimulation(-70.52, 48.19, 48.2, 10, 13, 0.25, 1.225, 0.075, 17.6);
        });
    }

    /*
     * test with valid drag-related parameters
     */
    @Test
    public void testValidDrag() 
    {
        assertDoesNotThrow(() -> {
            simulator.RunSimulation(70.52, 48.19, 48.2, 10, 13, 0.25, 1.225, 0.075, 17.6);
        });
    }

    /*
     * test with invalid drag-related parameters
     */
    @Test
    public void testInvalidDrag() 
    {
        assertThrows(IllegalArgumentException.class, () -> {
            simulator.RunSimulation(70.52, 48.19, 48.2, 100, 10, -1.0, 0, 0, 5);
        });
    }

    /*
     * test with edge case for initial height (min values)
     */
    @Test
    public void testEdgeCaseHeightVelocity() {
        assertDoesNotThrow(() -> {
            simulator.RunSimulation(70.52, 48.19, 48.2, 0, 13, 0.25, 1.225, 0.075, 17.6);
        });
    }
}
