package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PathTesting 
{

    private Simulator simulator;

    @BeforeEach
    public void setUp() {
        simulator = new Simulator(70.52, 48.19, 48.2);
    }

    /*
     * Path 1: Valid angles, valid drag parameters, valid ball mass, valid rocket turbo force, valid height and initial velocity, valid ball diameter.
     */
    @Test
    public void testValidInputs() 
    {
        assertDoesNotThrow(() -> simulator.RunSimulation(70.52, 48.19, 48.2, 10, 13, 0.25, 1.225, 0.075, 17.6));
    }

    /*
     * Path 2: Invalid angles.
     */
    @Test
    public void testInvalidAngles() 
    {
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(45, 30, 60, 100, 10, 0.5, 1.2, 10, 0.1));
    }

    /*
     * Path 3: Invalid drag parameters.
     */
    @Test
    public void testInvalidDragParameters() 
    {
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(-1.0, 0, 0, 100, 10, 0.5, 1.2, 10, 0.1));
    }

    /*
     * Path 4: Invalid ball mass.
     */
    @Test
    public void testInvalidBallMass() 
    {
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(45, 30, 60, 100, 10, 0.5, 1.2, 10, -0.1));
    }

    /*
     * Path 5: Invalid height and initial velocity.
     */
    @Test
    public void testInvalidHeightAndVelocity() 
    {
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(45, 30, 60, -100, 10, 0.5, 1.2, 10, 0.1));
    }

    /*
     * Path 6: Invalid ball diameter.
     */
    @Test
    public void testInvalidBallDiameter() 
    {
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(45, 30, 60, 100, 10, 0.5, 1.2, -10, 0.1));
    }
}
