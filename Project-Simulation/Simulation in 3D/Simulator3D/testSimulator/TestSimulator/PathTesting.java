package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PathTesting 
{
    private Simulator simulator;

    @BeforeEach
    public void setUp() {
        simulator = new Simulator(70.52, 48.19, 48.2);
    }

    /*
     * Path 1: Valid angles, valid height and initial velocity.
     */
    @Test
    public void testValidInputs() {
        // Test with valid angles and all other inputs within valid ranges
        assertDoesNotThrow(() -> simulator.ShowResults(70.52, 48.19, 48.2, 10, 13));
    }

    /*
     * Path 2: Invalid angles.
     */
    @Test
    public void testInvalidAngles() {
        assertThrows(IllegalArgumentException.class, () -> simulator.ShowResults(-45, 30, 60, 100, 10));
    }

    /*
     * Path 3: Invalid height and initial velocity.
     */
    @Test
    public void testInvalidHeightAndVelocity() {
        assertThrows(IllegalArgumentException.class, () -> simulator.ShowResults(70.52, 48.19, 48.2, -100, -10));
    }

    
}
