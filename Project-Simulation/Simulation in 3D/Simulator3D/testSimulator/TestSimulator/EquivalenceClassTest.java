package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EquivalenceClassTest
{
    private Simulator simulator;

    @BeforeEach
    public void setUp() {
        // Create a new Simulator instance before each test
        simulator = new Simulator(70.52, 48.19, 48.2);
    }

    @Test
    public void testValidAnglesEquivalence() {
        // Test with valid angles
        assertDoesNotThrow(() -> simulator.ShowResults(70.52, 48.19, 48.2, 10, 13));

        // Test with invalid angles
        assertThrows(IllegalArgumentException.class, () -> simulator.ShowResults(-10.0, 90.0, 190.0, 10, 13));
    }

    @Test
    public void testValidHeightEquivalence() {
        // Test with valid positive height
        assertDoesNotThrow(() -> simulator.ShowResults(70.52, 48.19, 48.2, 10, 13));

        // Test with height equal to 0
        assertDoesNotThrow(() -> simulator.ShowResults(70.52, 48.19, 48.2, 0, 13));

        // Test with invalid negative height
        assertThrows(IllegalArgumentException.class, () -> simulator.ShowResults(70.52, 48.19, 48.2, -10, 13));
    }
}
