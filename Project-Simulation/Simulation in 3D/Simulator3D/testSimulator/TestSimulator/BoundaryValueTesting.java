package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BoundaryValueTesting 
{

    private Simulator simulator;

    @BeforeEach
    public void setUp() {
        simulator = new Simulator(70.52, 48.19, 48.2);
    }

    @Test
    public void testShowResultsAnglesBoundary() {
        // Valid angles (boundary values)
        assertDoesNotThrow(() -> simulator.ShowResults(30.0, 45.0, 60.0, 0, 0));
        assertDoesNotThrow(() -> simulator.ShowResults(70.52, 48.19, 48.2, 10, 13));

        // Invalid angles (boundary values)
        assertThrows(IllegalArgumentException.class, () -> simulator.ShowResults(-70.52, 48.19, 48.2, 10, 13));
        assertThrows(IllegalArgumentException.class, () -> simulator.ShowResults(70.52, 48.19, -48.2, 10, 13));
    }

    @Test
    public void testShowResultsHeightBoundary() {
        // Valid height (boundary values)
        assertDoesNotThrow(() -> simulator.ShowResults(30.0, 45.0, 60.0, 0, 0));
        assertDoesNotThrow(() -> simulator.ShowResults(30.0, 45.0, 60.0, 100, 0));

        // Invalid height (boundary value)
        assertThrows(IllegalArgumentException.class, () -> simulator.ShowResults(0, 0, 0, -10, 0));
    }

    @Test
    public void testShowResultsInitialVelocityBoundary() {
        // Valid initial velocity (boundary values)
        assertDoesNotThrow(() -> simulator.ShowResults(70.52, 48.19, 48.2, 10, 13));
        assertDoesNotThrow(() -> simulator.ShowResults(70.52, 48.19, 48.2, 10, 3));

        // Invalid initial velocity (boundary value)
        assertThrows(IllegalArgumentException.class, () -> simulator.ShowResults(0, 0, 0, 10, -1));
    }
}
