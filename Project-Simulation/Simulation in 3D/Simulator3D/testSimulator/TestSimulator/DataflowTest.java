package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class DataflowTest 
{
    @Test
    public void testDataflowTesting() {
        // Dataflow Test 1: Valid input parameters
        Simulator simulator1 = new Simulator(70.52, 48.19, 48.2);
        assertDoesNotThrow(() -> simulator1.ShowResults(70.52, 48.19, 48.2, 10, 13));

        // Dataflow Test 2: Invalid angles
        assertThrows(IllegalArgumentException.class, () -> new Simulator(91, -100, 200).ShowResults(70.52, 48.19, 48.2, 10, 13));

        // Dataflow Test 3: Negative height and initial velocity
        assertThrows(IllegalArgumentException.class, () -> new Simulator(0, 45, 90).ShowResults(70.52, 48.19, 48.2, -100, -10));

        // Dataflow Test 4: Extreme values for input parameters
        Simulator simulator2 = new Simulator(70.52, 48.19, 48.2);
        assertDoesNotThrow(() -> simulator2.ShowResults(70.52, 48.19, 48.2, 1000, 2));
    }
}
