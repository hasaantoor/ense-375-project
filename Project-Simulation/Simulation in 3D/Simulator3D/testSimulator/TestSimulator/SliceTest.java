package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SliceTest 
{
    private Simulator simulator;

    /*
     * Create a new Simulator instance before each test
     */
    @BeforeEach
    public void setUp() {
        simulator = new Simulator(70.52, 48.19, 48.2);
    }

    /*
     * Test with valid angles
     */
    @Test
    public void testValidAngles() 
    {
        assertDoesNotThrow(() -> {
            simulator.ShowResults(70.52, 48.19, 48.2, 10, 13);
        });
    }

    /*
     * Test with invalid angles
     */
    @Test
    public void testInvalidAngles() 
    {
        assertThrows(IllegalArgumentException.class, () -> {
            simulator.ShowResults(-45, 30, 60, 100, 10);
        });
    }

    /*
     * Test with edge case for initial height (min values)
     */
    @Test
    public void testEdgeCaseHeightVelocity() {
        assertDoesNotThrow(() -> {
            simulator.ShowResults(70.52, 48.19, 48.2, 0, 13);
        });
    }
}
