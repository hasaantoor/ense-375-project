package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DecisionTableBasedTest 
{
    private Simulator simulator;

    /*
     * setup for Simulator() before each test
     */
    @BeforeEach
    public void setUp() {
        simulator = new Simulator(70.52, 48.19, 48.2);
    }

    /*
     * test for valid inputs
     */
    @Test
    public void testValidInput() {
        assertDoesNotThrow(() -> simulator.ShowResults(70.52, 48.19, 48.2, 100.0, 50.0));
    }

    /*
     * test for invalid height
     */
    @Test
    public void testInvalidHeight() {
        assertThrows(IllegalArgumentException.class, () -> simulator.ShowResults(70.52, 48.19, 48.2, -10.0, 50.0));
    }

    /*
     * test for invalid valocity
     */
    @Test
    public void testInvalidVelocity() {
        assertThrows(IllegalArgumentException.class, () -> simulator.ShowResults(70.52, 48.19, 48.2, 100.0, -10.0));
    }
}
