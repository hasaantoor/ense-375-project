package Simulator;

public class Simulator {
	public static final double	GRAVITY_ACCELERATION = 9.81;
	Point projectilePoint; 
    
	public Simulator (double a, double b, double c) {
		boolean validAngles = checkValidAngles(a, b, c);
		if(validAngles) {
			this.projectilePoint = new Point(0,0);
		}
		else {
			throw new IllegalArgumentException("Angles are not valid.");
		}
	};
	
	private boolean checkValidAngles(double a, double b, double c) {
		if (a >= 0 && b >= 0 && c >= 0) {
			a = Math.toRadians(a);
			b = Math.toRadians(b);
			c = Math.toRadians(c);
			int checkAngles = (int) Math.round(Math.pow(Math.cos(a), 2) + Math.pow(Math.cos(b), 2) + Math.pow(Math.cos(b), 2));
			return (checkAngles == 1);
			}
			else return false;
	};

	public double CalculateLinearVelocity(double externalForce, double drag, double weight, double mass) {
		
		double linearVelocity = (externalForce + drag + weight) / mass;
		return linearVelocity;
	};
	
	public double CalculateTotalTime(double alpha, double beta, double gamma, double initialPositionZ, double initialVelocity) {
	    double a = -0.5 * GRAVITY_ACCELERATION, b =  initialVelocity * Math.sin(Math.toRadians(gamma)), c = initialPositionZ;
	    double root1, root2 = 0;

	    double determinant = (b * b) - (4 * a * c);

	    if (determinant > 0) {
	      root1 = (-b + Math.sqrt(determinant)) / (2 * a);
	      root2 = (-b - Math.sqrt(determinant)) / (2 * a);
	    }
	    
	    else if (determinant == 0) {
	      root1 = root2 = -b / (2 * a);
	    }
	    else {
	      root1 = 0;
	    }
		
		return (root1 > 0 ? root1 : root2);
	};
	
	
	public void ShowResults(double a, double b, double c, double height, double initialVelocity) {
		if (a < 0 || b < 0 || c < 0) {
            throw new IllegalArgumentException("Angles must be non-negative.");
        }

        // Check for negative height and initialVelocity
        if (height < 0 || initialVelocity < 0) {
            throw new IllegalArgumentException("Height and initial velocity must be non-negative.");
        }
		
        System.out.println("Drag neglected, only initial velocity and g");
        System.out.printf("%-20s %-30s %-30s %-30s%n", "Time", "Distance-X", "Distance-Y", "Height-Z");
        double getTotalProjectileTime = CalculateTotalTime(a, b, c, height, initialVelocity);
        
		for(double time = 0; time <= getTotalProjectileTime; time += 0.5) {
			projectilePoint.GetValues(time, a, b, c, height, initialVelocity);
	        System.out.printf("%-20s %-30s %-30s %-30s%n", time, projectilePoint.position.finalX, projectilePoint.position.finalY, projectilePoint.position.finalZ);
		}
		
	}
	
}
