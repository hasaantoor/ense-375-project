package Simulator;

public class Velocity {
	double initial;
	double finalValue;
	
	
	public Velocity (double initial) {
		this.initial = initial;
	};
	
	public double CalculateVelocityComponenet (double velocity, double angle) {
  
        double radians = Math.toRadians(angle);
		return velocity * Math.cos(radians);
	};
	
}
