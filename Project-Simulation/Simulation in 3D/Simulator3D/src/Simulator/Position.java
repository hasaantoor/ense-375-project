package Simulator;

public class Position {
	double initialX;
	double initialY;
	double initialZ;
	double finalX;
	double finalY;
	double finalZ;
	
	
	public Position (double initialY) {
		this.initialZ = initialY;
		this.initialX = 0;
		this.initialY = 0;
	};	
	
	public double CalculatePositionX(double time, double initialVelocityX) {
		return initialX + (initialVelocityX * time);
	};
	
	public double CalculatePositionY(double time, double initialVelocityY) {
		return initialY + (initialVelocityY * time);
	};
	
	public double CalculatePositionZ(double time, double initialVelocityZ) {
		double height = (initialZ + (initialVelocityZ * time) - (0.5 * Simulator.GRAVITY_ACCELERATION * (time * time)));

		if(height > 0) {
			return height;
		}
		else{
			return 0.0;
		}
	};
}
