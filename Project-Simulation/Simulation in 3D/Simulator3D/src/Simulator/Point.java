package Simulator;

public class Point {
	double time;	
	Position position;
	Velocity velocity;

	public Point(double initialZ, double initialV) {
		this.time = 0;
		this.position = new Position(initialZ);
		this.velocity = new Velocity(initialV);
	};
	
	public Point GetValues(double time, double a, double b, double c, double initialZ, double initialV) {
		this.time = time;
		Point newPoint = new Point(initialZ, initialV);
		double vx0 = velocity.CalculateVelocityComponenet(initialV, a);
		double vy0 = velocity.CalculateVelocityComponenet(initialV, b);
		double vz0 = velocity.CalculateVelocityComponenet(initialV, c);

		this.position.finalX = position.CalculatePositionX(time, vx0);
		this.position.finalY = position.CalculatePositionY(time, vy0);		
		this.position.finalZ = position.CalculatePositionZ(time, vz0);		

		return newPoint;
	};
	
}
