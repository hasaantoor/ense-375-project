package Simulator;

public class Barrel {
	double a, b, c, height;
	
	public Barrel() {
		this.a = 0;
		this.b = 0;
		this.c = 0;
		this.height = 0;
	};
	
	public void SetBarrel(double a, double b, double c, double height) {
		if (a >= 0 && b >= 0 && c >= 0 && height >= 0) {
			this.a = a;
			this.b = b;
			this.c = c;
			this.height = height;
		};
	};
}
