package Simulator;

public class Position {
	double initialX;
	double initialY;
	double initialZ;
	double finalX;
	double finalY;
	double finalZ;
	
	
	public Position () {
		this.initialZ = 0;
		this.initialX = 0;
		this.initialY = 0;
	};	
	
	public void SetPosition (double initialZ) {
		this.initialZ = initialZ;
	};	
	
	public double CalculatePositionX(double time, double initialVelocityX, double acceleration) {
		return initialX + (initialVelocityX * time) + (0.5 * acceleration * (time * time));
	};
	
	public double CalculatePositionY(double time, double initialVelocityY, double acceleration) {
		return initialY + (initialVelocityY * time) + (0.5 * acceleration * (time * time));
	};
	
	public double CalculatePositionZ(double time, double initialVelocityZ, double acceleration) {
		double height = (initialZ + (initialVelocityZ * time) + (0.5 * acceleration * (time * time)));

		if(height > 0) {
			return height;
		}
		else{
			return 0.0;
		}
	};
}
