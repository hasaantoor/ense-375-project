package Simulator;

public class Acceleration {
	double initial;
	double x;
	double y;
	double z;
	
	public Acceleration () {
		this.initial = 0;
		this.x = 0;
		this.y = 0;
		this.z = 0;
	};
	
	public void CalculatAcceleration(Drag drag, Turbo rocket, double ballMass)
	{	      	      
        double[] totalForce = new double[3];
        totalForce[0] = drag.x + rocket.x;
        totalForce[1] = drag.y + rocket.y;
        totalForce[2] = (-ballMass * Simulator.GRAVITY_ACCELERATION) + drag.z + rocket.z;
        
        x = totalForce[0]/ballMass;
        y = totalForce[1]/ballMass;
        z = totalForce[2]/ballMass; 
	};
}
