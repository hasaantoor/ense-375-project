package Simulator;

public class Simulator {
	public static final double	GRAVITY_ACCELERATION = 9.81;
	Point projectilePoint; 
    Velocity v0;
    Drag drag;
    Turbo turbo;
    Acceleration acceleration;
    Projectile projectile;
    Barrel barrel;
    
	public Simulator (double a, double b, double c) {
		boolean validAngles = checkValidAngles(a, b, c);
		if(validAngles) {
			this.projectilePoint = new Point();
		    this.v0 = new Velocity();
		    this.drag = new Drag();
		    this.turbo = new Turbo();
		    this.acceleration = new Acceleration();
		    this.projectile = new Projectile();
		    this.barrel = new Barrel();
		    
		}
		else {
			throw new IllegalArgumentException("Angles are not valid.");
		}
	};
	
	private boolean checkValidAngles(double a, double b, double c) {
		/* This version of checkValidAngles cant handle negative values
		*/
		
		if (a >= 0 && b >= 0 && c >= 0) {
		a = Math.toRadians(a);
		b = Math.toRadians(b);
		c = Math.toRadians(c);
		int checkAngles = (int) Math.round(Math.pow(Math.cos(a), 2) + Math.pow(Math.cos(b), 2) + Math.pow(Math.cos(b), 2));
		return (checkAngles == 1);
		}
		else return false;
		

//       a = Math.abs(a);
//        b = Math.abs(b);
//        c = Math.abs(c);
//        return (a <= 90 && b <= 90 && c <= 90 && (a + b + c) == 180);
    
	};
	
	public double CalculateTotalTime() {
	    double a = 0.5 * acceleration.z, b =  v0.vy, c = barrel.height;
	    double root1, root2 = 0;

	    double determinant = (b * b) - (4 * a * c);

	    if (determinant > 0) {
	      root1 = (-b + Math.sqrt(determinant)) / (2 * a);
	      root2 = (-b - Math.sqrt(determinant)) / (2 * a);
	    }
	    
	    else if (determinant == 0) {
	      root1 = root2 = -b / (2 * a);
	    }
	    else {
	      root1 = 0;
	    }
		
		return (root1 > 0 ? root1 : root2);
	};
	
	public void RunSimulation(double a, double b, double c, double height, double initialVelocity, double dragCoefficient, double fluidDensity, double ballDiameter, double ballMass, double rocketTurboForce) {
        boolean validAngles = checkValidAngles(a, b, c);
        if (validAngles && height >= 0 && initialVelocity >= 0 && dragCoefficient >= 0 && fluidDensity >= 0 && ballDiameter >= 0 && ballMass >= 0 && rocketTurboForce >= 0) {
            projectilePoint.SetPoint(height, initialVelocity);
            v0.SetVelocity(initialVelocity);
            drag.SetDrag(dragCoefficient, fluidDensity);
            turbo.SetTurbo(rocketTurboForce);
            projectile.SetProjectile(ballMass, ballDiameter);
            barrel.SetBarrel(a, b, c, height);

            ShowResults();
        } else {
            throw new IllegalArgumentException("Invalid input parameters.");
        }
    };
	
	public void CalculateVectors(double time) {
		projectile.CalculateArea();
		double timeMax = CalculateTotalTime();
		
		v0.SetVelocityComponents(v0.initial, barrel.a, barrel.b, barrel.c);
		turbo.CalculateTurbo(turbo.initial, barrel.a, barrel.b, barrel.c, time, timeMax);
		drag.CalculateDrag(drag.dragCoefficent, drag.fluidDensity, projectile.area, v0, projectile.mass);
		acceleration.CalculatAcceleration(drag, turbo, projectile.mass);
	};
	
	public void ShowResults() {
        System.out.println("Rocket turbo, drag, and g forces");
        System.out.printf("%-20s %-30s %-30s %-30s%n", "Time", "Distance-X", "Distance-Y", "Height-Z");

		double time = 0;
		do {
				CalculateVectors(time);
				projectilePoint.GetValues(time, v0, barrel.height, acceleration);
				System.out.printf("%-20s %-30s %-30s %-30s%n", time, projectilePoint.position.finalX, projectilePoint.position.finalY, projectilePoint.position.finalZ);
				time += 0.5;
			}while(projectilePoint.position.finalZ > 0 || time < 2.0);
		}
	
}
