package Simulator;

public class Turbo {
	double initial;
	double x;
	double y;
	double z;
	
	public Turbo () {
		this.initial = 0;
		this.x = 0;
		this.y = 0;
		this.z = 0;
	};
	
	public void SetTurbo (double initial) {
		if (initial >= 0) {
			this.initial = initial;
		};
	};
	
	public void CalculateTurbo(double rocketTurboForce, double a, double b, double c, double time, double timeMax) {
		
		if(time >= timeMax/2) {
			rocketTurboForce = 0;
		}
		x = rocketTurboForce * Math.cos(Math.toRadians(a));
		y = rocketTurboForce * Math.cos(Math.toRadians(b));
		z = rocketTurboForce * Math.cos(Math.toRadians(c));
		
	};	
}
