package Simulator;

public class Velocity {
	double initial;
	double vx;
	double vy;
	double vz;	
	
	public Velocity () {
		this.initial = 0;
	};
	
	public void SetVelocity (double initial) {
		if(initial >= 0) {
			this.initial = initial;
		}
	};
	
	public void SetVelocityComponents(double initialVelocity, double a, double b, double c) {
		initial = initialVelocity;
		vx = CalculateVelocityComponenet(initialVelocity, a);
		vy = CalculateVelocityComponenet(initialVelocity, b);
		vz = CalculateVelocityComponenet(initialVelocity, c);
	};

	
	public double CalculateVelocityComponenet (double velocity, double angle) {
		
		if (angle < (double)-90.0) {
			System.err.println("Angle is not valid. Automatically adjusting to -90 degrees.");
			angle = -90.0;
		} else if (angle > (double)90.0) {
			System.err.println("Angle is not valid. Automatically adjusting to 90 degrees.");
			angle = 90.0;
		}
		
        double radians = Math.toRadians(angle);
		return velocity * Math.cos(radians);
	};
	
	public double CalculateVelocity (double time, double initialVelocity, double acceleration) {
		  
		return initialVelocity + (acceleration * time);
	};
	
}
