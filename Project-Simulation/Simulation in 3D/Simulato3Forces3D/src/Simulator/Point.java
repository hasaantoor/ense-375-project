package Simulator;

public class Point {
	double time;	
	Position position;
	Velocity velocity;

	public Point() {
		this.position = new Position();
		this.velocity = new Velocity();
	};
	
	public void SetPoint(double initialZ, double initialV) {
		position.SetPosition(initialZ);
		velocity.SetVelocity(initialV);
	};
	
	public Point GetValues(double time, Velocity v0, double initialZ, Acceleration acceleration) {
		this.time = time;
		this.position.finalX = position.CalculatePositionX(time, v0.vx, acceleration.x);
		this.position.finalY = position.CalculatePositionY(time, v0.vy, acceleration.y);		
		this.position.finalZ = position.CalculatePositionZ(time, v0.vz, acceleration.z);		

		this.velocity.vx = velocity.CalculateVelocity(time, v0.vx, acceleration.x);		
		this.velocity.vy = velocity.CalculateVelocity(time, v0.vy, acceleration.y);
		this.velocity.vz = velocity.CalculateVelocity(time, v0.vz, acceleration.z);		

		return this;
	};
	
}
