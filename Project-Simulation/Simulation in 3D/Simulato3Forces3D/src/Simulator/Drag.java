package Simulator;

public class Drag {
	double x;
	double y;
	double z;
	double dragCoefficent, fluidDensity;
	
	public Drag () {
		this.x = 0;
		this.y = 0;
		this.z = 0;
		this.dragCoefficent = 0;
		this.fluidDensity = 0;
	};
	
	public void SetDrag (double dragCoefficent, double fluidDensity) {
		this.dragCoefficent = dragCoefficent;
		this.fluidDensity = fluidDensity;
	};
	
	public void CalculateDrag (double dragCoefficent, double fluidDensity, 
			double projectileArea, Velocity v0, double ballMass)
	{	
		x = -0.5 * dragCoefficent * fluidDensity * projectileArea * Math.pow(v0.vx, 2);
		y = -0.5 * dragCoefficent * fluidDensity * projectileArea * Math.pow(v0.vy, 2);
		z = -0.5 * dragCoefficent * fluidDensity * projectileArea * Math.pow(v0.vz, 2);       
	};	
}
