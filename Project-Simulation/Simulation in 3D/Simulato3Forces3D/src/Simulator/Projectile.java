package Simulator;

public class Projectile {
	double mass;
	double diameter;
	double area;
	
	public Projectile () {
		this.mass = 0;
		this.diameter = 0;
		this.area = 0;
	};
	
	public void SetProjectile (double mass, double diameter) {
		if (mass >= 0 && diameter >= 0) {			
			this.mass = mass;
			this.diameter = diameter;
			this.area = 0;
		}
	};
	
	public void CalculateArea() {
		this.area = 0.25 * Math.PI * Math.pow(diameter, 2);

	};
}
