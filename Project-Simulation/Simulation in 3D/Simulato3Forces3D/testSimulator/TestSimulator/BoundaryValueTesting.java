package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BoundaryValueTesting 
{
	private Simulator simulator;

    @BeforeEach
    public void setUp() 
	{
        simulator = new Simulator(70.52, 48.19, 48.2);
    }

    @Test
    public void testValidAnglesBoundary() 
	{	
    	// Robust Boundary Testing 
    	
    	// xmin - 0.1
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(-0.1, 30.0, 120.0, 100.0, 30, 0.47, 1.225, 0.18, 17.6, 20));
        // xmin
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(0.0, 30.0, 120.0, 100.0, 30, 0.47, 1.225, 0.18, 17.6, 20));
        // xmin + 0.1
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(0.1, 30.0, 120.0, 100.0, 30, 0.47, 1.225, 0.18, 17.6, 20));
        
        // xnom
        assertDoesNotThrow(() -> simulator.RunSimulation(70.52, 48.19, 48.2, 100.0, 30, 0.47, 1.225, 0.18, 17.6, 20));
        
        // xmax - 0.1
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(30.0, 30.0, 179.9, 100.0, 30, 0.47, 1.225, 0.18, 17.6, 20));
        // xmax
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(30.0, 30.0, 180.0, 100.0, 30, 0.47, 1.225, 0.18, 17.6, 20));
        // xmax + 0.1
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(30.0, 30.0, 180.1, 100.0, 30, 0.47, 1.225, 0.18, 17.6, 20));
    }

    @Test
    public void testHeightBoundary() 
	{
        assertDoesNotThrow(() -> simulator.RunSimulation(70.52, 48.19, 48.2, 0.0, 50.0, 0.2, 1.2, 0.2, 0.05, 20));
        assertDoesNotThrow(() -> simulator.RunSimulation(70.52, 48.19, 48.2, 10000.0, 30, 0.47, 1.225, 0.18, 17.6, 20));

        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(70.52, 48.19, 48.2, -0.1, 50.0, 0.2, 1.2, 0.2, 0.05, 20));
    }
}
