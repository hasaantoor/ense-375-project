package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DecisionTableBasedTest 
{
    private Simulator simulator;

    /*
     * setup for Simulator() before each test
     */
    @BeforeEach
    public void setUp() 
    {
        simulator = new Simulator(70.52, 48.19, 48.2);
    }

    /*
     * test for valid inputs
     */
    @Test
    public void testValidInput() {
        assertDoesNotThrow(() -> simulator.RunSimulation(70.52, 48.19, 48.2, 100.0, 50.0, 0.2, 1.2, 0.2, 0.05, 1000.0));
    }

    /*
     * test for invalid height
     */
    @Test
    public void testInvalidHeight() {
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(70.52, 48.19, 48.2, -10.0, 50.0, 0.2, 1.2, 0.2, 0.05, 1000.0));
    }

    /*
     * test for invalid valocity
     */
    @Test
    public void testInvalidVelocity() {
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(70.52, 48.19, 48.2, 100.0, -10.0, 0.2, 1.2, 0.2, 0.05, 1000.0));
    }

    /*
     * test for invalid drag
     */
    @Test
    public void testInvalidDragCoefficient() {
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(70.52, 48.19, 48.2, 100.0, 50.0, -0.1, 1.2, 0.2, 0.05, 1000.0));
    }

    /*
     * test for invalid fluid
     */
    @Test
    public void testInvalidFluidDensity() {
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(70.52, 48.19, 48.2, 100.0, 50.0, 0.2, -0.1, 0.2, 0.05, 1000.0));
    }

    /*
     * test for invalid ball diameter
     */
    @Test
    public void testInvalidBallDiameter() {
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(30.0, 30.0, 120.0, 100.0, 50.0, 0.2, 1.2, -0.2, 0.05, 1000.0));
    }

    /*
     * test for invalid ball mass
     */
    @Test
    public void testInvalidBallMass() {
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(70.52, 48.19, 48.2, 100.0, 50.0, 0.2, 1.2, 0.2, -0.1, 1000.0));
    }

    /*
     * test for invalid turbo force
     */
    @Test
    public void testInvalidTurboForce() {
        assertThrows(IllegalArgumentException.class, () -> simulator.RunSimulation(70.52, 48.19, 48.2, 100.0, 50.0, 0.2, 1.2, 0.2, 0.05, -1000.0));
    }
}
