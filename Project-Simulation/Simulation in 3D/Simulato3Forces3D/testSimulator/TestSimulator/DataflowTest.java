package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class DataflowTest 
{
    @Test
    public void testDataflowTesting() 
    {
        // Dataflow Test 1: Valid input parameters
        Simulator simulator1 = new Simulator(70.52, 48.19, 48.2);
        assertDoesNotThrow(() -> simulator1.RunSimulation(70.52, 48.19, 48.2, 10, 13, 0.25, 1.225, 0.075, 17.6, 20));

        // Dataflow Test 2: Invalid angles
        assertThrows(IllegalArgumentException.class, () -> new Simulator(91, -100, 200).RunSimulation(70.52, 48.19, 48.2, 10, 13, 0.25, 1.225, 0.075, 17.6, 20));

        // Dataflow Test 3: Negative height and initial velocity
        assertThrows(IllegalArgumentException.class, () -> new Simulator(0, 45, 90).RunSimulation(70.52, 48.19, 48.2, 100, 10, 0.5, 1.2, 10, 0.1, 20));

        // Dataflow Test 4: Invalid ball mass and negative rocket turbo force
        assertThrows(IllegalArgumentException.class, () -> new Simulator(0, 45, 90).RunSimulation(70.52, 48.19, 48.2, 100, 10, 0.5, 1.2, 10, -0.1, 0));

        // Dataflow Test 5: Zero ball mass and zero rocket turbo force
        assertThrows(IllegalArgumentException.class, () -> new Simulator(0, 45, 90).RunSimulation(70.52, 48.19, 48.2, 100, 10, 0.5, 1.2, 10, 0, 0));

        // Dataflow Test 6: Extreme values for input parameters
        Simulator simulator2 = new Simulator(70.52, 48.19, 48.2);
        assertDoesNotThrow(() -> simulator2.RunSimulation(70.52, 48.19, 48.2, 1000, 2, 0.1, 0.1, 1, 1, 1));
    }
}
