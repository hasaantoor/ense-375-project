package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class IntegrationTesting {
    
	@Test
	void testPointClass() {
		// Integration testing on the Point Class in a Top Down Approach, providing test stubs for what would normally be the Position and Velocity Classes.
		
		// test driver
		Point point = new Point();
		
		// test stub 1
		Velocity velocity = new Velocity();
		velocity.SetVelocityComponents(5, 5, 5, 5);
		
		// test stub 2
		Acceleration acceleration = new Acceleration();
		
		assertDoesNotThrow(() -> point.GetValues(0, velocity, 0, acceleration));
		
		
	}

}
