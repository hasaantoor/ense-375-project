package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SliceTest 
{
    private Simulator simulator;

    /*
     *Create a new Simulator instance before each test
     */
    @BeforeEach
    public void setUp() {

        simulator = new Simulator(70.52, 48.19, 48.2);
    }
    /*
     * Test with valid angels
     */
    @Test
    public void testValidAngles() 
    {
        assertDoesNotThrow(() -> {
            simulator.RunSimulation(70.52, 48.19, 48.2, 10, 13, 0.25, 1.225, 0.075, 17.6, 20);
        });
    }

    /*
     * test with invalid angles
     */
    @Test
    public void testInvalidAngles() 
    {
        assertThrows(IllegalArgumentException.class, () -> {
            new Simulator(91, -100, 200);
        });
    }

    /*
     * test with valid drag-related parameters
     */
    @Test
    public void testValidDrag() 
    {
        assertDoesNotThrow(() -> {
            simulator.RunSimulation(70.52, 48.19, 48.2, 10, 13, 0.25, 1.225, 0.075, 17.6, 20);
        });
    }

    /*
     * test with invalid drag-related parameters
     */
    @Test
    public void testInvalidDrag() 
    {
        assertThrows(IllegalArgumentException.class, () -> {
            simulator.RunSimulation(70.52, 48.19, 48.2, 100, 10, -1.0, 0, 0, -5, 1000);
        });
    }

    /*
     * Test with valid rocket turbo force
     */
    @Test
    public void testValidRocket() 
    {
        assertDoesNotThrow(() -> {
            simulator.RunSimulation(70.52, 48.19, 48.2, 10, 13, 0.25, 1.225, 0.075, 17.6, 2);
        });
    }

    /*
     * test with invalid rocket turbo force
     */
    @Test
    public void testInvalidRocket() {
        assertThrows(IllegalArgumentException.class, () -> {
            simulator.RunSimulation(70.52, 48.19, 48.2, 10, 13, 0.25, 1.225, 0.075, 17.6, -20);
        });
    }

    /*
     * test with edge case for initial height (min values)
     */
    @Test
    public void testEdgeCaseHeightVelocity() {
        assertDoesNotThrow(() -> {
            simulator.RunSimulation(70.52, 48.19, 48.2, 0, 13, 0.25, 1.225, 0.075, 17.6, 20);
        });
    }

    /*
     * Test with edge case min value for rocket
     */
    @Test
    public void testMinRocket() 
    {
        assertDoesNotThrow(() -> {
            simulator.RunSimulation(70.52, 48.19, 48.2, 10, 13, 0.25, 1.225, 0.075, 17.6, 0);
        });
    }
}
