package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestSimulator {

	@Test
	public void testSimulator ()
	{
		Simulator simulator = new Simulator(70.52, 48.19, 48.2);
		assertDoesNotThrow(() -> simulator.RunSimulation(70.52, 48.19, 48.2, 10, 13, 0.25, 1.225, 0.075, 17.6, 20));
	};	
	
	@Test
	public void testSimulator2 ()
	{
		Simulator simulator = new Simulator(70.52, 48.19, 48.2);
		assertDoesNotThrow(() -> simulator.RunSimulation(70.52, 48.19, 48.2, 0, 30, 0.25, 1.225, 0.075, 2, 20));
	};
	
	@Test
	public void testSimulator3 ()
	{
		Simulator simulator = new Simulator(70.52, 48.19, 48.2);
		assertDoesNotThrow(() -> simulator.RunSimulation(70.52, 48.19, 48.2, 0, 30, 0.47, 1.225, 0.18, 17.6, 20));
	};

		
}
