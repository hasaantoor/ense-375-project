package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestSimulator {

	@Test
	public void testSimulator ()
	{
		Simulator simulator = new Simulator(55, 10, 13);
		simulator.ShowResults(55, 10, 13, 0.25, 1.225, 0.075, 17.6);
	};	
	
	@Test
	public void testSimulator2 ()
	{
		Simulator simulator = new Simulator(45, 0, 30);
		simulator.ShowResults(45, 0, 30, 0.25, 1.225, 0.075, 17.6);
	};
	
	@Test
	public void testSimulator3 ()
	{
		Simulator simulator = new Simulator(45, 0, 30);
		simulator.ShowResults(45, 0, 30, 0.47, 1.23, 0.18, 17.6);
	};

		
}
