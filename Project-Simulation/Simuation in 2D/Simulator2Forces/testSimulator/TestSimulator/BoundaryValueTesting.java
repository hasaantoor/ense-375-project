package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BoundaryValueTesting 
{
    /*
     * this test focuses on the minimum angle by setting the angle value to 0.0
     */
	@Test
    public void testMinimumAngle() 
    {
        Simulator simulator = new Simulator(0.0, 10.0, 20.0);
        simulator.ShowResults(0.0, 10.0, 20.0, 0.2, 1.2, 0.2, 0.1);
    }

    /*
     * this test focuses on the maximum angle by setting the angle value to 90.0
     */
    @Test
    public void testMaximumAngle() 
    {
        Simulator simulator = new Simulator(90.0, 10.0, 20.0);
        simulator.ShowResults(90.0, 10.0, 20.0, 0.2, 1.2, 0.2, 0.1);
    }

    /*
     * this test focuses on the minimum height by setting the angle value to 0.0
     */
    @Test
    public void testMinimumHeight() 
    {
        Simulator simulator = new Simulator(45.0, 0.0, 20.0);
        simulator.ShowResults(45.0, 0.0, 20.0, 0.2, 1.2, 0.2, 0.1);
    }

    /*
     * this test focuses on the minimum initial velocity by setting the initiaVelocity value to 0.0
     */
    @Test
    public void testMinimumInitialVelocity() 
    {
        Simulator simulator = new Simulator(45.0, 10.0, 0.0);
        simulator.ShowResults(45.0, 10.0, 0.0, 0.2, 1.2, 0.2, 0.1);
    }

    /*
     * this test focuses on the maximum initial velocity by setting the initiaVelocity value to 1000.0
     */
    @Test
    public void testMaximumInitialVelocity() 
    {
        Simulator simulator = new Simulator(45.0, 10.0, 1000.0);
        simulator.ShowResults(45.0, 10.0, 1000.0, 0.2, 1.2, 0.2, 0.1);
    }

}
