package Simulator;
import Simulator.*;
public class Position {
	double initialX;
	double initialY;
	double finalX;
	double finalY;
	
	
	public Position (double initialY) {
		this.initialY = initialY;
		this.initialX = 0;
	};	
	
	public double CalculatePositionDragX(double time, double initialVelocityX, double a) {
		double distance = (initialX + (initialVelocityX * time) + (0.5 * a * (time * time)));

		if(distance > 0) {
			return distance;
		}
		else{
			return 0.0;
		}	};
	
	public double CalculatePositionDragY(double time, double initialVelocityY, double a) {
		double height = (initialY + (initialVelocityY * time) + (0.5 * a * (time * time)));

		if(height > 0) {
			return height;
		}
		else{
			return 0.0;
		}
	};
}
