package TestSimulator;
import Simulator.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BoundaryValueTesting 
{
    //delta is used to check if the actual value is within range, if less than or equal to DELTA then the assertion passes. or else it will fail
    private final double DELTA = 1e-8;

    
    @Test
    public void testCalculateTotalTime() {
        Simulator simulator = new Simulator(45.0, 10.0, 20.0);

        // Test when angle, initialPositionY, and initialVelocity are positive
        double totalTime = simulator.CalculateTotalTime(45.0, 10.0, 20.0);
        assertEquals(3.4706329174929915, totalTime, DELTA);

        // Test when angle, initialPositionY, and initialVelocity are zero
        totalTime = simulator.CalculateTotalTime(0.0, 0.0, 0.0);
        assertEquals(0.0, totalTime, DELTA);

        // Test when angle, initialPositionY, and initialVelocity are negative
        totalTime = simulator.CalculateTotalTime(-45.0, -10.0, -20.0);
        assertEquals(1.2428926355524126, totalTime, DELTA);
    }

    @Test
    public void testCalculateExternalForce() {
        Simulator simulator = new Simulator(45.0, 10.0, 20.0);

        // Test with valid input values
        double[] acceleration = simulator.CalculateExternalForce(45.0, 0.2, 1.2, 0.1, 10.0, 0.5, 5.0);
        assertArrayEquals(new double[]{5.871067811865475, -3.938932188134525}, acceleration, DELTA);

        // Test with negative input values
        acceleration = simulator.CalculateExternalForce(-45.0, -0.2, -1.2, -0.1, -10.0, -0.5, -5.0);
        assertArrayEquals(new double[]{5.871067811865475, -18.081067811865474}, acceleration, DELTA);

        // Test with zero input values
        acceleration = simulator.CalculateExternalForce(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        //assertArrayEquals(new double[]{0.0, -9.81}, acceleration, DELTA);
    }
}
