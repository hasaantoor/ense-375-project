package Simulator;
import Simulator.*;
//import java.util.*;
public class Simulator {
	public static final double	GRAVITY_ACCELERATION = 9.81;
	Point projectilePoint; 
    
	public Simulator (double angle, double height, double initialVelocity) {
		this.projectilePoint = new Point(height, initialVelocity);
	};
	

	public double CalculateLinearVelocity(double externalForce, double drag, double weight, double mass) {
		
		double linearVelocity = (externalForce + drag + weight) / mass;
		return linearVelocity;
	};
	
	public double CalculateTotalTime(double angle, double initialPositionY, double initialVelocity) {
	    double a = -0.5 * GRAVITY_ACCELERATION, b =  initialVelocity * Math.sin(Math.toRadians(angle)), c = initialPositionY;
	    double root1, root2 = 0;

	    double determinant = (b * b) - (4 * a * c);

	    if (determinant > 0) {
	      root1 = (-b + Math.sqrt(determinant)) / (2 * a);
	      root2 = (-b - Math.sqrt(determinant)) / (2 * a);
	    }
	    
	    else if (determinant == 0) {
	      root1 = root2 = -b / (2 * a);
	    }
	    else {
	      double real = -b / (2 * a);
	      double imaginary = Math.sqrt(-determinant) / (2 * a);
	      root1 = 0;
	    }
		
		return (root1 > 0 ? root1 : root2);
	};
	
	public double[] CalculateExternalForce (double angle, double dragCoefficent, double fluidDensity, double projectileArea, double flowVelocity, double ballMass, double rocketTurboForce)
	{		
        double vx = flowVelocity * Math.cos(Math.toRadians(angle));
        double vy = flowVelocity * Math.sin(Math.toRadians(angle));

		double dragX = -0.5 * dragCoefficent * fluidDensity * projectileArea * Math.pow(vx, 2);
		double dragY = -0.5 * dragCoefficent * fluidDensity * projectileArea * Math.pow(vy, 2);

		double rocketX = rocketTurboForce * Math.cos(Math.toRadians(angle));
		double rocketY = rocketTurboForce * Math.sin(Math.toRadians(angle));
		
        double[] force = new double[2];
        force[0] = dragX + rocketX;
        force[1] = (-ballMass * Simulator.GRAVITY_ACCELERATION) + dragY + rocketY;
        	
        double[] acceleration = new double[2];
        acceleration[0] = force[0]/ballMass;
        acceleration[1] = force[1]/ballMass;
        
		return acceleration;
	};
	
	public void ShowResults(double angle, double height, double initialVelocity, double dragCoefficent, double fluidDensity, double ballDiameter, double ballMass, double rocketTurboForce) {
        System.out.println("External Force neglected, drag and g");
        System.out.printf("%-20s %-30s %-30s%n", "Time", "Distance", "Height");
        double getTotalProjectileTime = CalculateTotalTime(angle, height, initialVelocity);
		
		for(double time = 0; time <= getTotalProjectileTime; time += 0.5) {
			double projectileArea = 0.25 * Math.PI * Math.pow(ballDiameter, 2);
			double[] acceleration = CalculateExternalForce(angle, dragCoefficent, fluidDensity, projectileArea, initialVelocity, ballMass, rocketTurboForce);
			projectilePoint.GetValues(time, angle, height, initialVelocity, acceleration);
	        System.out.printf("%-20s %-30s %-30s%n", time, projectilePoint.position.finalX, projectilePoint.position.finalY);
		}
	}
	
}
