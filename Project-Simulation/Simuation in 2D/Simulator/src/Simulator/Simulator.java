package Simulator;
import Simulator.*;
//import java.util.*;
public class Simulator {
	public static final double	GRAVITY_ACCELERATION = 9.81;
	Point projectilePoint; 
    
	public Simulator (double angle, double height, double initialVelocity) {
		this.projectilePoint = new Point(height, initialVelocity);
	};
	

	public double CalculateLinearVelocity(double externalForce, double drag, double weight, double mass) {
		
		double linearVelocity = (externalForce + drag + weight) / mass;
		return linearVelocity;
	};
	
	public double CalculateTotalTime(double angle, double initialPositionY, double initialVelocity) {
	    double a = -0.5 * GRAVITY_ACCELERATION, b =  initialVelocity * Math.sin(Math.toRadians(angle)), c = initialPositionY;
	    double root1, root2 = 0;

	    double determinant = (b * b) - (4 * a * c);

	    if (determinant > 0) {
	      root1 = (-b + Math.sqrt(determinant)) / (2 * a);
	      root2 = (-b - Math.sqrt(determinant)) / (2 * a);
	    }
	    
	    else if (determinant == 0) {
	      root1 = root2 = -b / (2 * a);
	    }
	    else {
	      double real = -b / (2 * a);
	      double imaginary = Math.sqrt(-determinant) / (2 * a);
	      root1 = 0;
	    }
		
		return (root1 > 0 ? root1 : root2);
	};
	
	public void RunSimulator(double externalForce, double mass, int dragCoefficent, double fluidDensity, double projectileArea, double flowVelocity)  {
		/*
		 * double projectileDrag = CalculateDrag(dragCoefficent, fluidDensity,
		 * projectileArea, flowVelocity); double weight = CalculateWeight(mass); double
		 * linearVelocity = CalculateLinearVelocity(externalForce, projectileDrag,
		 * weight, mass); double projectilePosition = linearVelocity;
		 */
		
	};
	

	
	public void ShowResults(double angle, double height, double initialVelocity) {
        System.out.println("Drag neglected, only initial velocity and g");
        System.out.printf("%-20s %-30s %-30s%n", "Time", "Distance", "Height");
        double getTotalProjectileTime = CalculateTotalTime(angle, height, initialVelocity);
        
		for(double time = 0; time <= getTotalProjectileTime; time += 0.5) {
			projectilePoint.GetValues(time, angle, height, initialVelocity);
	        System.out.printf("%-20s %-30s %-30s%n", time, projectilePoint.position.finalX, projectilePoint.position.finalY);
		}
	}
	
}
