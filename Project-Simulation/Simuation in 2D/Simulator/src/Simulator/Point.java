package Simulator;
import Simulator.*;

public class Point {
	double time;	
	Position position;
	Velocity velocity;
    double ballMass;      // Ball mass
    double ballCrossSectionArea;      // Area of the ball cross-section

	public Point(double initialY, double initialV) {
		this.time = 0;
		this.position = new Position(initialY);
		this.velocity = new Velocity(initialV);
	};
	
	public Point GetValues(double time, double angle, double initialY, double initialV) {
		this.time = time;
		Point newPoint = new Point(initialY, initialV);
		double vx0 = velocity.CalculateVelocityX(initialV, angle);
		double vy0 = velocity.CalculateVelocityY(initialV, angle);

		this.position.finalX = position.CalculatePositionX(time, vx0);
		this.position.finalY = position.CalculatePositionY(time, vy0);		
		
		return newPoint;
	};
	
}
