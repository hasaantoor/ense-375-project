package Simulator;
import Simulator.*;
public class Position {
	double initialX;
	double initialY;
	double finalX;
	double finalY;
	
	
	public Position (double initialY) {
		this.initialY = initialY;
		this.initialX = 0;
	};	
	
	public double CalculatePositionX(double time, double initialVelocityX) {
		return initialX + (initialVelocityX * time);
	};
	
	public double CalculatePositionY(double time, double initialVelocityY) {
		double height = (initialY + (initialVelocityY * time) - (0.5 * Simulator.GRAVITY_ACCELERATION * (time * time)));

		if(height > 0) {
			return height;
		}
		else{
			return 0.0;
		}
	};
}
