package Simulator;
import java.util.*;

public class Velocity {
	double initial;
	double finalValue;
	
	
	public Velocity (double initial) {
		this.initial = initial;
	};
	
	public double CalculateVelocityX (double velocity, double angle) {
  
        double radians = Math.toRadians(angle);
		return velocity * Math.cos(radians);
	};
	
	public double CalculateVelocityY (double velocity, double angle) {
		  
        double radians = Math.toRadians(angle);
		return velocity * Math.sin(radians);
	};
	
}
